"""
File: eight_bit_avatar_cli_version.py
---------------------------
This program gets a series of color and style selections from the user and then pops up a window
with their customized avatar at the end.
"""
import tkinter


CANVAS_WIDTH = 800
CANVAS_HEIGHT = 800
CANVAS_COLOR = '#03fcd3'
ACCEPTABLE_ANSWERS = ['yes', 'y', 'no', 'n']

# The following constants allow me to draw the avatar by using the number of pixels (squares) that make up the avatar
# starting from the base avatar's upper left corner position on the canvas
PIXEL_WIDTH = 50
PIXEL_HEIGHT = 50
STARTING_X = 200
STARTING_Y = 150

# feature colors and styles
EYE_COLORS = {
    'blue': '#88CBF1',
    'brown': '#5C3201',
    'green': '#6F9061',
    'grey': '#939393',
    'amber': '#BB823E'
    }
SKIN_COLORS = {
    'fair': ('#FAE6DB', '#EEA2A9'),
    'medium': ('#d3aa91', '#CA8D7C'),
    'olive': ('#ba8762', '#b16b47'),
    'brown': ('#4c2e1e', '#47271a'),
    'black': ('#31261D', '#40342B')
    }
HAIR_COLORS = {
    'blond': '#F6E49A',
    'light brown': '#A77D5D',
    'brown': '#584129',
    'black': '#000000',
    'natural red': '#CA7429',
    'grey': '#C1C1C1',
    'pink': '#FC01C7',
    'purple': '#610882',
    'blue': '#1F4EFD',
    'green': '#41D002',
    'yellow': '#FCF401',
    'orange': '#ff6523',
    'red': '#DE0101'
    }
HAIR_STYLES = {
    'short': 1,
    'medium': 2,
    'long': 3,
    'ponytail': 4,
    'pigtails': 5,
    'bald': 6,
    'mohawk': 7
    }
TOP_STYLES = {
    'plain': 1,
    'striped': 2,
    'suspenders': 3,
    'jacket': 4
    }


# This function was provided by the instructors.
# It creates a window that contains a drawing canvas that can
# be used to make drawings.
def make_canvas(width, height, title):
    """
    DO NOT MODIFY
    Creates and returns a drawing canvas
    of the given int size,
    ready for drawing.
    """
    top = tkinter.Tk()
    top.minsize(width=width, height=height)
    top.title(title)
    canvas = tkinter.Canvas(top, bg=CANVAS_COLOR, width=width + 1, height=height + 1)
    canvas.pack()
    return canvas


def draw_rows(canvas, x_range_start, x_range_end, pix_y, pixel_color):
    for pix_x in range(x_range_start, x_range_end):
        upper_left_x = STARTING_X + (pix_x * PIXEL_WIDTH)
        upper_left_y = STARTING_Y + (pix_y * PIXEL_HEIGHT)
        lower_right_x = STARTING_X + ((pix_x + 1) * PIXEL_WIDTH)
        lower_right_y = STARTING_Y + ((pix_y + 1) * PIXEL_HEIGHT)
        canvas.create_rectangle(upper_left_x, upper_left_y, lower_right_x, lower_right_y,
                                fill=pixel_color, outline=pixel_color)


def draw_columns(canvas, x_range_start, y_range_start, x_range_end, y_range_end, pixel_color):
    for pix_y in range(y_range_start, y_range_end):
        draw_rows(canvas, x_range_start, x_range_end, pix_y, pixel_color)


def draw_body(canvas, skin_colors_pair, eye_color):
    draw_head(canvas, skin_colors_pair)
    draw_columns(canvas, 0, 10, 1, 13, skin_colors_pair[0])  # left arm
    draw_columns(canvas, 7, 10, 8, 13, skin_colors_pair[0])  # right arm
    draw_eyes(canvas, eye_color)


def draw_head(canvas, skin_colors_pair):
    draw_columns(canvas, 2, 1, 6, 2, skin_colors_pair[0])   # forehead
    draw_columns(canvas, 0, 4, 1, 6, skin_colors_pair[0])   # left ear
    draw_columns(canvas, 1, 2, 7, 7, skin_colors_pair[0])   # central face
    draw_columns(canvas, 7, 4, 8, 6, skin_colors_pair[0])   # right ear
    draw_columns(canvas, 2, 7, 6, 8, skin_colors_pair[0])   # chin
    draw_columns(canvas, 3, 8, 5, 10, skin_colors_pair[0])  # neck
    draw_columns(canvas, 3, 6, 5, 7, skin_colors_pair[1])   # mouth


def draw_eyes(canvas, eye_color):
    draw_columns(canvas, 2, 4, 3, 5, eye_color)  # left eye
    draw_columns(canvas, 5, 4, 6, 5, eye_color)  # right eye


def draw_short_hair(canvas, hair_color):
    draw_columns(canvas, 0, 1, 1, 6, hair_color)        # far left side
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)        # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 6, hair_color)        # far right side


def draw_medium_hair(canvas, hair_color):
    draw_columns(canvas, -2, 4, -1, 7, hair_color)    # far left side
    draw_columns(canvas, -1, 3, 0, 8, hair_color)
    draw_columns(canvas, 0, 1, 1, 9, hair_color)
    draw_columns(canvas, 1, 1, 2, 4, hair_color)
    draw_columns(canvas, 1, 7, 2, 8, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)      # center
    draw_columns(canvas, 6, 1, 7, 4, hair_color)
    draw_columns(canvas, 6, 7, 7, 8, hair_color)
    draw_columns(canvas, 7, 1, 8, 9, hair_color)
    draw_columns(canvas, 8, 3, 9, 8, hair_color)
    draw_columns(canvas, 9, 4, 10, 7, hair_color)     # far right side


def draw_long_hair(canvas, hair_color):
    draw_columns(canvas, 0, 2, 1, 10, hair_color)       # far left side
    draw_columns(canvas, 1, 1, 2, 4, hair_color)
    draw_columns(canvas, 1, 7, 2, 10, hair_color)
    draw_columns(canvas, 2, 8, 3, 9, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)        # center
    draw_columns(canvas, 5, 8, 6, 9, hair_color)
    draw_columns(canvas, 6, 7, 7, 10, hair_color)
    draw_columns(canvas, 6, 1, 7, 4, hair_color)
    draw_columns(canvas, 7, 2, 8, 10, hair_color)       # far right side


def draw_ponytail(canvas, hair_color):
    draw_columns(canvas, 0, 1, 1, 6, hair_color)        # far left side
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)        # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 6, hair_color)
    draw_columns(canvas, 8, 2, 9, 4, hair_color)        # far right side
    draw_columns(canvas, 8, 4, 9, 6, '#FFFFFF')         # hair tie
    draw_columns(canvas, 9, 4, 10, 5, '#FFFFFF')        # hair tie
    draw_columns(canvas, 9, 5, 11, 9, hair_color)
    draw_columns(canvas, 8, 8, 10, 11, hair_color)      # ponytail
    draw_columns(canvas, 9, 11, 10, 12, hair_color)


def draw_pigtails(canvas, hair_color):
    draw_columns(canvas, -3, 1, -2, 4, hair_color)      # left pigtail
    draw_columns(canvas, -2, 1, -1, 5, hair_color)
    draw_columns(canvas, -1, 2, 1, 3, '#FFFFFF')        # hair tie
    draw_columns(canvas, 0, 1, 1, 5, hair_color)
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)        # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 5, hair_color)
    draw_columns(canvas, 8, 2, 9, 3, '#FFFFFF')         # hair tie
    draw_columns(canvas, 9, 1, 10, 5, hair_color)
    draw_columns(canvas, 10, 1, 11, 4, hair_color)      # right pigtail


def draw_bald(canvas, hair_color):
    draw_columns(canvas, -1, 1, 0, 4, hair_color)       # far left side
    draw_columns(canvas, 0, 0, 1, 6, hair_color)
    draw_columns(canvas, 1, 0, 2, 3, hair_color)
    draw_columns(canvas, 6, 0, 7, 3, hair_color)
    draw_columns(canvas, 7, 0, 8, 6, hair_color)
    draw_columns(canvas, 8, 1, 9, 4, hair_color)        # far right side


def draw_mohawk(canvas, hair_color):
    draw_columns(canvas, 3, -2, 5, 3, hair_color)


def draw_beard(canvas, hair_color):
    canvas.create_rectangle(325, 425, 475, 475, fill=hair_color, outline=hair_color)    # over lips
    canvas.create_rectangle(350, 500, 450, 525, fill=hair_color, outline=hair_color)    # under lips
    canvas.create_rectangle(225, 475, 350, 525, fill=hair_color, outline=hair_color)    # left of mouth
    canvas.create_rectangle(450, 475, 575, 525, fill=hair_color, outline=hair_color)    # right of mouth
    canvas.create_rectangle(275, 525, 525, 575, fill=hair_color, outline=hair_color)    # chin cover


def draw_glasses(canvas, hair_color):
    frame_color = '#960101' if hair_color == '#000000' else '#000000'
    canvas.create_rectangle(275, 300, 360, 325, fill=frame_color, outline=frame_color)  # left: top
    canvas.create_rectangle(250, 325, 275, 425, fill=frame_color, outline=frame_color)  # left: left
    canvas.create_rectangle(275, 425, 360, 450, fill=frame_color, outline=frame_color)  # left: bottom
    canvas.create_rectangle(360, 325, 385, 425, fill=frame_color, outline=frame_color)  # left: right
    canvas.create_rectangle(385, 375, 415, 400, fill=frame_color, outline=frame_color)  # bridge
    canvas.create_rectangle(440, 300, 525, 325, fill=frame_color, outline=frame_color)  # right: top
    canvas.create_rectangle(415, 325, 440, 425, fill=frame_color, outline=frame_color)  # right: left
    canvas.create_rectangle(440, 425, 525, 450, fill=frame_color, outline=frame_color)  # right: bottom
    canvas.create_rectangle(525, 325, 550, 425, fill=frame_color, outline=frame_color)  # right: right


def draw_plain_shirt(canvas):
    draw_columns(canvas, 2, 9, 6, 10, '#6C6C6C')      # shirt collar
    draw_columns(canvas, 1, 10, 7, 13, '#6C6C6C')     # shirt body
    draw_columns(canvas, 0, 10, 1, 12, '#7C7C7C')     # left sleeve
    draw_columns(canvas, 7, 10, 8, 12, '#7C7C7C')     # right sleeve


def draw_striped_shirt(canvas):
    draw_columns(canvas, 2, 9, 6, 10, '#FFFFFF')      # shirt collar
    draw_columns(canvas, 1, 10, 7, 11, '#000000')     # shirt body
    draw_columns(canvas, 1, 11, 7, 12, '#FFFFFF')     # shirt body
    draw_columns(canvas, 1, 12, 7, 13, '#000000')     # shirt body


def draw_suspenders(canvas):
    draw_columns(canvas, 0, 10, 1, 12, '#8A8A8A')  # left sleeve
    draw_columns(canvas, 1, 10, 2, 13, '#C10000')  # left suspender
    draw_columns(canvas, 2, 9, 3, 10, '#A0A0A0')   # left collar
    draw_columns(canvas, 2, 10, 6, 13, '#A0A0A0')  # shirt center
    draw_columns(canvas, 5, 9, 6, 10, '#A0A0A0')   # right collar
    draw_columns(canvas, 6, 10, 7, 13, '#C10000')  # right suspender
    draw_columns(canvas, 7, 10, 8, 12, '#8A8A8A')  # right sleeve


def draw_jacket(canvas):
    draw_columns(canvas, 2, 9, 3, 10, '#3A3A3A')   # collar left
    draw_columns(canvas, 5, 9, 6, 10, '#3A3A3A')   # collar right
    draw_columns(canvas, 1, 10, 3, 13, '#666665')  # left side jacket
    draw_columns(canvas, 5, 10, 7, 13, '#666665')  # right side jacket
    draw_columns(canvas, 3, 10, 5, 13, '#FFFFFF')  # undershirt
    draw_columns(canvas, 0, 10, 1, 13, '#3A3A3A')  # left sleeve
    draw_columns(canvas, 7, 10, 8, 13, '#3A3A3A')  # right sleeve


def draw_top(canvas, top_style):
    switcher = {
        1: draw_plain_shirt,
        2: draw_striped_shirt,
        3: draw_suspenders,
        4: draw_jacket
    }
    return switcher.get(top_style, lambda: 'Invalid top')(canvas)


def draw_hair(canvas, hair_style, hair_color):
    switcher = {
        1: draw_short_hair,
        2: draw_medium_hair,
        3: draw_long_hair,
        4: draw_ponytail,
        5: draw_pigtails,
        6: draw_bald,
        7: draw_mohawk
    }
    return switcher.get(hair_style, lambda: 'Invalid hair')(canvas, hair_color)


def color_style_selector(attribute, options_dict):
    print('\n\tPlease select ' + attribute + ' from the following list: ')
    count = 1
    for key in options_dict:
        print(f'\t\t\t{count}. {key}')
        count += 1
    choice = (input('\t\tEnter your selection: ')).lower()
    while choice not in options_dict:
        choice = (input('That was not on the list. Please enter a valid option: ')).lower()
    return options_dict[choice]


def is_feature_wanted(prompt):
    want_feature = (input(prompt)).lower()
    while want_feature not in ACCEPTABLE_ANSWERS:
        want_feature = (input('Please answer yes or no: ')).lower()
    feature = True if (want_feature == 'yes' or want_feature == 'y') else False
    return feature


def draw_avatar(canvas, top_style, hair_style, hair_color, beard, skin_colors_pair, eye_color, glasses):
    draw_body(canvas, skin_colors_pair, eye_color)
    draw_hair(canvas, hair_style, hair_color)
    draw_top(canvas, top_style)
    if beard:
        draw_beard(canvas, hair_color)
    if glasses:
        draw_glasses(canvas, hair_color)


def main():

    print('\n\t\t|-------------------------------------------|')
    print('\t\t|     Welcome to the 8-bit Avatar Maker.    |')
    print('\t\t|-------------------------------------------|\n')
    print('\t\tYou will begin by choosing your color options.')
    print('\t\tThen your custom avatar will be drawn for you!')
    print('\t\t\t\t\t\tHAVE FUN!!!')

    eye_color = color_style_selector('an eye color', EYE_COLORS)
    skin_colors_pair = color_style_selector('a skin tone', SKIN_COLORS)
    hair_color = color_style_selector('a hair color', HAIR_COLORS)
    hair_style = color_style_selector('a hair style', HAIR_STYLES)
    top_style = color_style_selector('a top style', TOP_STYLES)

    beard_prompt = '\n\tWould you like your avatar to have a beard? (y/n) '
    beard = is_feature_wanted(beard_prompt)

    glasses_prompt = '\n\tWould you like your avatar to wear glasses? (y/n) '
    glasses = is_feature_wanted(glasses_prompt)

    canvas = make_canvas(CANVAS_WIDTH, CANVAS_HEIGHT, 'my 8-bit avatar')
    draw_avatar(canvas, top_style, hair_style, hair_color, beard, skin_colors_pair, eye_color, glasses)

    print('\n------------------------------------------------------------------------------')
    print('\t\t\t\t\t\t\t~~~ BEHOLD ~~~')
    print('\tIn the window that just opened is your brand new, customized avatar!!!')
    print('------------------------------------------------------------------------------')
    while True:
        canvas.update()

        # save_prompt = '\nWould you like to save your avatar? (y/n) '
        # save = is_feature_wanted(save_prompt)
        # if save:
        #     save_as_png(canvas, 'my_avatar')

        close_canvas = input('\n\t==> Press RETURN when you are ready to close the avatar window <==')
        if close_canvas == '':
            raise SystemExit


if __name__ == '__main__':
    main()
